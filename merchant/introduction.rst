===========================
Taler merchant introduction
===========================

This file will include all documentation for the first video tutorial in the Taler merchant series.


++++++++++++
Requirements 
++++++++++++

Required for this tutorial series:

* | A Taler merchant backend
  | For convenience, we recommend you to use https://backend.demo.taler.net.



* | A Taler wallet instance
  | Setup instructions are available on https://wallet.taler.net, though not required for this introcuction tutorial.



* | A REST API management software or relevant programming skills
  | In this video series, we are going to use the Insomnia free software, available for download at https://insomnia.rest.
  | As an introduction, this video will show how to export working code from your prepared queries in Insomnia in Python (or any supported language) and execute them, but this will not be demonstrated again in the next tutorials.


+++++++++++++++++++
Demo backend notice 
+++++++++++++++++++

Using the online Taler merchant demo service https://backend.demo.taler.net is strongly recommended for this tutorial.

  | The access key is: **sandbox**.


++++++++++++++++++++++++++++++++++
Information about the secret token 
++++++++++++++++++++++++++++++++++

The secret token is compliant with the RFC 8959 document. You can learn more about it here: https://datatracker.ietf.org/doc/html/rfc8959.
Keep in mind that this is a Bearer token, and that your authoirization header needs to specify it.

  | Your authorization header should look like this:
  | **Authorization: Bearer secret-token:<access key>**

|
|
|



